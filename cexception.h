#ifndef CEXCEPTION_H_
#define CEXCEPTION_H_

#include <time.h>
#include <stdbool.h>
#include <limits.h>
#include <setjmp.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

/**
 * Copyright (C) 2009-2013 Francesco Nidito
 * Copyright (c) 2013 William Roberts
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * --------------------------------------------------------------------------------------------------
 *
 * Rule of thumb, if it starts with an _ (underscore) don't call it, or you're on your own.
 *
 * Throw catch mechanism was adopted from http://www.di.unipi.it/~nids/docs/longjump_try_trow_catch.html
 * and is licensed under the MIT License.
 *
 * You can have nested TRY blocks, start threads, and it should all work as expected with the following
 * caveats:
 * 1. You cannot jump (ie goto) out of a TRY/CATCH block, doing so will cause the FINALLY block not to execute.
 * 2. If starting a thread from within a TRY block, if the new thread THROW's an error, and no TRY block
 *    was encountered in that thread, the resulting exception will not end up where the thread was
 *    created from. The exception will propagate back to the global runtime exception handler, and
 *    a panic will ensue.
 * 3. You cannot call THROW from within a CATCH, you must use CTHROW if you would like to do that. This
 *    is really just a more specific version of 1. If you call CTHROW the finally block WILL NOT EXECUTE.
 *
 * For implementation details, scroll down some more.
 *
 * A large thanks goes to Francesco for his help in incorporating this into our project and for
 * the code itself. The code below is not Francesco's original implementation, but rather a heavily
 * modified version of it.
 *
 * EXAMPLE TRY BLOCK
 * void func1() {
 *
 *	TRY {
 *		TRY {
 *			TRY {
 *				THROW(EXCEPTION1);
 *
 *			}
 *			CATCH(EXCEPTION1) {
 *				cexception_dump_error();
 *				CTHROW(EXCEPTION1); //Notice the CTHROW since it's in a catch
 *			}
 *			FINALLY {}
 *			ETRY;
 *		}
 *		CATCH(EXCEPTION1) {
 *			cexception_dump_error();
 *			CTHROW(EXCEPTION1); //Notice the CTHROW since its in a catch
 *		}
 *		FINALLY {}
 *		ETRY;
 *	}
 *	CATCH(EXCEPTION1) {
 *		cexception_dump_error();
 *	}
 *	FINALLY { } //Needed even though empty
 *	ETRY
 *	return;
 * }
 *
 */

typedef enum cexception cexception;
typedef struct cexception_map cexception_map;

struct cexception_map {
	int err;
	char *name;
	char *msg;
};

/**
 * Different error numbers in the error
 * subsystem.
 *
 */
enum cexception {
	cexception_none = 0,
	cexception_e2big = E2BIG,
	cexception_eacces = EACCES,
	cexception_eaddrinuse = EADDRINUSE,
	cexception_eaddrnotavail = EADDRNOTAVAIL,
	cexception_eafnosupport = EAFNOSUPPORT,
	cexception_eagain = EAGAIN,
	cexception_ewouldblock = EWOULDBLOCK,
	cexception_ealready = EALREADY,
	cexception_ebade = EBADE,
	cexception_ebadf = EBADF,
	cexception_ebadfd = EBADFD,
	cexception_ebadmsg = EBADMSG,
	cexception_ebadr = EBADR,
	cexception_ebadrqc = EBADRQC,
	cexception_ebadslt = EBADSLT,
	cexception_ebusy = EBUSY,
	cexception_ecanceled = ECANCELED,
	cexception_echild = ECHILD,
	cexception_echrng = ECHRNG,
	cexception_ecomm = ECOMM,
	cexception_econnaborted = ECONNABORTED,
	cexception_econnrefused = ECONNREFUSED,
	cexception_econnreset = ECONNRESET,
	cexception_edeadlk = EDEADLK,
	cexception_edeadlock = EDEADLOCK,
	cexception_edestaddrreq = EDESTADDRREQ,
	cexception_edom = EDOM,
	cexception_edquot = EDQUOT,
	cexception_eexist = EEXIST,
	cexception_efault = EFAULT,
	cexception_efbig = EFBIG,
	cexception_ehostdown = EHOSTDOWN,
	cexception_ehostunreach = EHOSTUNREACH,
	cexception_eidrm = EIDRM,
	cexception_eilseq = EILSEQ,
	cexception_einprogress = EINPROGRESS,
	cexception_eintr = EINTR,
	cexception_einval = EINVAL,
	cexception_eio = EIO,
	cexception_eisconn = EISCONN,
	cexception_eisdir = EISDIR,
	cexception_eisnam = EISNAM,
	cexception_ekeyexpired = EKEYEXPIRED,
	cexception_ekeyrejected = EKEYREJECTED,
	cexception_ekeyrevoked = EKEYREVOKED,
	cexception_el2hlt = EL2HLT,
	cexception_el2nsync = EL2NSYNC,
	cexception_el3hlt = EL3HLT,
	cexception_el3rst = EL3RST,
	cexception_elibacc = ELIBACC,
	cexception_elibbad = ELIBBAD,
	cexception_elibmax = ELIBMAX,
	cexception_elibscn = ELIBSCN,
	cexception_elibexec = ELIBEXEC,
	cexception_eloop = ELOOP,
	cexception_emediumtype = EMEDIUMTYPE,
	cexception_emfile = EMFILE,
	cexception_emlink = EMLINK,
	cexception_emsgsize = EMSGSIZE,
	cexception_emultihop = EMULTIHOP,
	cexception_enametoolong = ENAMETOOLONG,
	cexception_enetdown = ENETDOWN,
	cexception_enetreset = ENETRESET,
	cexception_enetunreach = ENETUNREACH,
	cexception_enfile = ENFILE,
	cexception_enobufs = ENOBUFS,
	cexception_enodata = ENODATA,
	cexception_enodev = ENODEV,
	cexception_enoent = ENOENT,
	cexception_enoexec = ENOEXEC,
	cexception_enokey = ENOKEY,
	cexception_enolck = ENOLCK,
	cexception_enolink = ENOLINK,
	cexception_enomedium = ENOMEDIUM,
	cexception_enomem = ENOMEM,
	cexception_enomsg = ENOMSG,
	cexception_enonet = ENONET,
	cexception_enopkg = ENOPKG,
	cexception_enoprotoopt = ENOPROTOOPT,
	cexception_enospc = ENOSPC,
	cexception_enosr = ENOSR,
	cexception_enostr = ENOSTR,
	cexception_enosys = ENOSYS,
	cexception_enotblk = ENOTBLK,
	cexception_enotconn = ENOTCONN,
	cexception_enotdir = ENOTDIR,
	cexception_enotempty = ENOTEMPTY,
	cexception_enotsock = ENOTSOCK,
	cexception_enotsup = ENOTSUP,
	cexception_enotty = ENOTTY,
	cexception_enotuniq = ENOTUNIQ,
	cexception_enxio = ENXIO,
	cexception_eopnotsupp = EOPNOTSUPP,
	cexception_eoverflow = EOVERFLOW,
	cexception_eperm = EPERM,
	cexception_epfnosupport = EPFNOSUPPORT,
	cexception_epipe = EPIPE,
	cexception_eproto = EPROTO,
	cexception_eprotonosupport = EPROTONOSUPPORT,
	cexception_eprototype = EPROTOTYPE,
	cexception_erange = ERANGE,
	cexception_eremchg = EREMCHG,
	cexception_eremote = EREMOTE,
	cexception_eremoteio = EREMOTEIO,
	cexception_erestart = ERESTART,
	cexception_erofs = EROFS,
	cexception_eshutdown = ESHUTDOWN,
	cexception_espipe = ESPIPE,
	cexception_esocktnosupport = ESOCKTNOSUPPORT,
	cexception_esrch = ESRCH,
	cexception_estale = ESTALE,
	cexception_estrpipe = ESTRPIPE,
	cexception_etime = ETIME,
	cexception_etimedout = ETIMEDOUT,
	cexception_etxtbsy = ETXTBSY,
	cexception_euclean = EUCLEAN,
	cexception_eunatch = EUNATCH,
	cexception_eusers = EUSERS,
	cexception_exdev = EXDEV,
	cexception_exfull = EXFULL,
	/*
	 * ADD YOUR EXCEPTION HERE cexception_myexception = -1
	 * (note your err num needs to be unique, negative is preferred)
	 */
};

cexception_map __errmap[] = {
		{ cexception_e2big, "cexception_e2big", NULL },
		{ cexception_eacces, "cexception_eacces", NULL },
		{ cexception_eaddrinuse, "cexception_eaddrinuse", NULL },
		{ cexception_eaddrnotavail, "cexception_eaddrnotavail", NULL },
		{ cexception_eafnosupport, "cexception_eafnosupport", NULL },
		{ cexception_eagain, "cexception_eagain", NULL },
		{ cexception_ewouldblock, "cexception_ewouldblock", NULL },
		{ cexception_ealready, "cexception_ealready", NULL },
		{ cexception_ebade, "cexception_ebade", NULL },
		{ cexception_ebadf, "cexception_ebadf", NULL },
		{ cexception_ebadfd, "cexception_ebadfd", NULL },
		{ cexception_ebadmsg, "cexception_ebadmsg", NULL },
		{ cexception_ebadr, "cexception_ebadr", NULL },
		{ cexception_ebadrqc, "cexception_ebadrqc", NULL },
		{ cexception_ebadslt, "cexception_ebadslt", NULL },
		{ cexception_ebusy, "cexception_ebusy", NULL },
		{ cexception_ecanceled, "cexception_ecanceled", NULL },
		{ cexception_echild, "cexception_echild", NULL },
		{ cexception_echrng, "cexception_echrng", NULL },
		{ cexception_ecomm, "cexception_ecomm", NULL },
		{ cexception_econnaborted, "cexception_econnaborted", NULL },
		{ cexception_econnrefused, "cexception_econnrefused", NULL },
		{ cexception_econnreset, "cexception_econnreset", NULL },
		{ cexception_edeadlk, "cexception_edeadlk", NULL },
		{ cexception_edeadlock, "cexception_edeadlock", NULL },
		{ cexception_edestaddrreq, "cexception_edestaddrreq", NULL },
		{ cexception_edom, "cexception_edom", NULL },
		{ cexception_edquot, "cexception_edquot", NULL },
		{ cexception_eexist, "cexception_eexist", NULL },
		{ cexception_efault, "cexception_efault", NULL },
		{ cexception_efbig, "cexception_efbig", NULL },
		{ cexception_ehostdown, "cexception_ehostdown", NULL },
		{ cexception_ehostunreach, "cexception_ehostunreach", NULL },
		{ cexception_eidrm, "cexception_eidrm", NULL },
		{ cexception_eilseq, "cexception_eilseq", NULL },
		{ cexception_einprogress, "cexception_einprogress", NULL },
		{ cexception_eintr, "cexception_eintr", NULL },
		{ cexception_einval, "cexception_einval", NULL },
		{ cexception_eio, "cexception_eio", NULL },
		{ cexception_eisconn, "cexception_eisconn", NULL },
		{ cexception_eisdir, "cexception_eisdir", NULL },
		{ cexception_eisnam, "cexception_eisnam", NULL },
		{ cexception_ekeyexpired, "cexception_ekeyexpired", NULL },
		{ cexception_ekeyrejected, "cexception_ekeyrejected", NULL },
		{ cexception_ekeyrevoked, "cexception_ekeyrevoked", NULL },
		{ cexception_el2hlt, "cexception_el2hlt", NULL },
		{ cexception_el2nsync, "cexception_el2nsync", NULL },
		{ cexception_el3hlt, "cexception_el3hlt", NULL },
		{ cexception_el3rst, "cexception_el3rst", NULL },
		{ cexception_elibacc, "cexception_elibacc", NULL },
		{ cexception_elibbad, "cexception_elibbad", NULL },
		{ cexception_elibmax, "cexception_elibmax", NULL },
		{ cexception_elibscn, "cexception_elibscn", NULL },
		{ cexception_elibexec, "cexception_elibexec", NULL },
		{ cexception_eloop, "cexception_eloop", NULL },
		{ cexception_emediumtype, "cexception_emediumtype", NULL },
		{ cexception_emfile, "cexception_emfile", NULL },
		{ cexception_emlink, "cexception_emlink", NULL },
		{ cexception_emsgsize, "cexception_emsgsize", NULL },
		{ cexception_emultihop, "cexception_emultihop", NULL },
		{ cexception_enametoolong, "cexception_enametoolong", NULL },
		{ cexception_enetdown, "cexception_enetdown", NULL },
		{ cexception_enetreset, "cexception_enetreset", NULL },
		{ cexception_enetunreach, "cexception_enetunreach", NULL },
		{ cexception_enfile, "cexception_enfile", NULL },
		{ cexception_enobufs, "cexception_enobufs", NULL },
		{ cexception_enodata, "cexception_enodata", NULL },
		{ cexception_enodev, "cexception_enodev", NULL },
		{ cexception_enoent, "cexception_enoent", NULL },
		{ cexception_enoexec, "cexception_enoexec", NULL },
		{ cexception_enokey, "cexception_enokey", NULL },
		{ cexception_enolck, "cexception_enolck", NULL },
		{ cexception_enolink, "cexception_enolink", NULL },
		{ cexception_enomedium, "cexception_enomedium", NULL },
		{ cexception_enomem, "cexception_enomem", NULL },
		{ cexception_enomsg, "cexception_enomsg", NULL },
		{ cexception_enonet, "cexception_enonet", NULL },
		{ cexception_enopkg, "cexception_enopkg", NULL },
		{ cexception_enoprotoopt, "cexception_enoprotoopt", NULL },
		{ cexception_enospc, "cexception_enospc", NULL },
		{ cexception_enosr, "cexception_enosr", NULL },
		{ cexception_enostr, "cexception_enostr", NULL },
		{ cexception_enosys, "cexception_enosys", NULL },
		{ cexception_enotblk, "cexception_enotblk", NULL },
		{ cexception_enotconn, "cexception_enotconn", NULL },
		{ cexception_enotdir, "cexception_enotdir", NULL },
		{ cexception_enotempty, "cexception_enotempty", NULL },
		{ cexception_enotsock, "cexception_enotsock", NULL },
		{ cexception_enotsup, "cexception_enotsup", NULL },
		{ cexception_enotty, "cexception_enotty", NULL },
		{ cexception_enotuniq, "cexception_enotuniq", NULL },
		{ cexception_enxio, "cexception_enxio", NULL },
		{ cexception_eopnotsupp, "cexception_eopnotsupp", NULL },
		{ cexception_eoverflow, "cexception_eoverflow", NULL },
		{ cexception_eperm, "cexception_eperm", NULL },
		{ cexception_epfnosupport, "cexception_epfnosupport", NULL },
		{ cexception_epipe, "cexception_epipe", NULL },
		{ cexception_eproto, "cexception_eproto", NULL },
		{ cexception_eprotonosupport, "cexception_eprotonosupport", NULL },
		{ cexception_eprototype, "cexception_eprototype", NULL },
		{ cexception_erange, "cexception_erange", NULL },
		{ cexception_eremchg, "cexception_eremchg", NULL },
		{ cexception_eremote, "cexception_eremote", NULL },
		{ cexception_eremoteio, "cexception_eremoteio", NULL },
		{ cexception_erestart, "cexception_erestart", NULL },
		{ cexception_erofs, "cexception_erofs", NULL },
		{ cexception_eshutdown, "cexception_eshutdown", NULL },
		{ cexception_espipe, "cexception_espipe", NULL },
		{ cexception_esocktnosupport, "cexception_esocktnosupport", NULL },
		{ cexception_esrch, "cexception_esrch", NULL },
		{ cexception_estale, "cexception_estale", NULL },
		{ cexception_estrpipe, "cexception_estrpipe", NULL },
		{ cexception_etime, "cexception_etime", NULL },
		{ cexception_etimedout, "cexception_etimedout", NULL },
		{ cexception_etxtbsy, "cexception_etxtbsy", NULL },
		{ cexception_euclean, "cexception_euclean", NULL },
		{ cexception_eunatch, "cexception_eunatch", NULL },
		{ cexception_eusers, "cexception_eusers", NULL },
		{ cexception_ewouldblock, "cexception_ewouldblock", NULL },
		{ cexception_exdev, "cexception_exdev", NULL },
		{ cexception_exfull, "cexception_exfull", NULL },
		/*
		 * ADD YOUR EXCEPTION HERE: { number, name and msg }
		 * { cexception_myexception, "cexception_myexception", "MY EXCEPTION MSG" },
		 */
};


typedef enum _cexception_stack_action _cexception_stack_action;
typedef struct _cexception _cexception;
typedef struct _cexception_stack _cexception_stack;
typedef struct _cexception_details _cexception_details;

/**
 * Nodes for a stack of jmp_buf's from setjmp. Used to implement the
 * try catch finally macros.
 */
struct _cexception_stack {
		jmp_buf jbuf;
		_cexception_stack *next;
};

/**
 * Details of the last active error
 */
struct _cexception_details {
	int active;
	cexception num;
	int line;
	const char *file;
	const char *func;
};

struct _cexception {
	_cexception_stack *head;      //!< Head pointer to stack of jump bufs
	_cexception_details curr_err; //!< Current error being thrown
};

/**
 * PEEK or POP from the error stack
 */
enum _cexception_stack_action {
	_cexception_stack_peek = 0, //!< used in __error_pop to denote a "peek" operation
	_cexception_stack_pop       //!< used in __error_pop to denote a "pop" operation
};


/**
 * TRY's some code out, on a THROW, the function returns to the nearest CATCH block.
 */
#define TRY \
	{ int exception; _cexception_stack __env = { .next = NULL }; __error_push(&__env); switch( exception = setjmp(__env.jbuf) ){ case 0: while(1){

/**
 * CATCH catches an exception
 * @param x
 *  The exception value to catch, one of cexception, corresponds with THROW(e)
 *  You can get the current value through _e_, declared as int.
 */
#define CATCH(x) break; case x:

/**
 * CATCH block that catches ALL exceptions (up to INT_MAX)
 * @warning
 *  Requires GNUC as it depends on a ranged case statement.
 */
#define CATCHALL break; case INT_MIN ... -1: case 1 ... INT_MAX:

/**
 * Always runs after the TRY/CATCH blocks, assuming no one exits the program
 * @note Must be included
 */
#define FINALLY break; } default: { __error_pop(_cexception_stack_pop); }

/**
 * Ends a TRY BLOCK, the whole thing
 *
 */
#define ETRY } }

/**
 * Throws an exception to be caught by a CATCH block.
 * @param e
 *  The exception number to throw, one of cexception
 */
#define THROW(e) \
		do { __set_active_error(__LINE__, __FILE__, __func__, e); \
		longjmp(__error_pop(_cexception_stack_peek)->jbuf, e); \
		} while (0)

/**
 * Throws an exception to be caught by a CATCH block FROM INSIDE A CATCH BLOCK!!
 * @param e
 *  The exception number to throw, one of cexception
 */
#define CTHROW(e) \
		do { __error_pop(_cexception_stack_pop); \
		__set_active_error(__LINE__, __FILE__, __func__, e); \
		longjmp(__error_pop(_cexception_stack_peek)->jbuf, e); \
		} while (0)

/**
 * Thread Local Storage for per thread error stack
 * @warning
 *  Ties us heavily to GCC
 *  DONT MODIFY THIS DIRECTLY
 *
 * @note
 *  Static initializes to all 0's
 */
__thread _cexception __tls_exception__;

/**
 * Thread Local Storage for per thread error stack
 * @warning
 *  DONT MODIFY THIS DIRECTLY
 *
 * @note
 *  Static initializes to all 0's
 */
_cexception __exception__;

/**
 * Dumps the last error seen by the system to stdout and
 * clears the last seen error. Ie two consecutive calls without
 * a THROW in-between will cause NO output on the second call.
 *
 */
static void cexception_dump_error() {

	int i;
	int found = 0;
	char *msg = NULL;
	char *name = NULL;
	_cexception *err = __tls_exception__.head ? &(__tls_exception__) : &(__exception__);

	if(err->curr_err.active) {

		for(i=0; i < (sizeof(__errmap)/sizeof(__errmap[0])); i++) {
			if (err->curr_err.num  == __errmap[i].err) {
				msg = __errmap[i].msg;
				name = __errmap[i].name;
				found = 1;
				break;
			}
		}

		if(!found) {
			msg = "Error number not found in lookup!";
			name= "";
		}
		else {
			if(!msg) {
				msg = strerror(err->curr_err.num);
			}
			if(!name) {
				name = "";
			}
		}

		fprintf(stderr, "Error %s -- \"%s\" in file: %s in function: %s on line %d\n",
			name,
			msg,
			err->curr_err.file,
			err->curr_err.func,
			err->curr_err.line);

		err->curr_err.active = 0;
	}

	return;
}

 /**
  * A call to this function results in the program terminating through
  * a call to exit(). Exit is passed the argument to this function.
  * Before exit() is called, the currently active error is printed, as
  * if you called cexception_dump_error() prior to calling exit().
  *
  * TODO Panic should be configurable, perhaps call a user registered
  * function with some userdata pointer.
  *
  * @param rc
  *  The return code for exit() to use.
  */
static void cexception_panic(int rc) {
	fprintf(stderr, "PANIC -- Uncaught Exception\n");
	cexception_dump_error();
	exit(rc);
}

/*
 * Begins needed declarations for the TRY/CATCH mechanism that should not be called
 * or used in any way. Using these will result in undefined behavior. With that said,
 * lets discuss the implementation:
 *
 * The TRY is a setjmp with some extra logic, the THROW is longjmp, and the others are some
 * sugar to make it all meld together.
 *
 * Their are 2 global variables used to maintain 2 stacks. One stack uses a thread specific
 * variable to store the head pointer and currently thrown error. The other stack, is a program
 * wide (ie all threads) variable that is used to hold the global error information. The split
 * stacks allows the protected runtime to set the jmp point for any thread created outside of it
 * back to the initial try catch in main().
 *
 * Each TRY pushes a jmpbuf (for setjmp/longjmp) onto the stack. If the stack head for the thread
 * is NULL, the push goes to the program scoped stack. This ensures that any thread created from
 * cexception_start() will have an associated error handler. Once the program global stack is initialized,
 * subsequent TRY's push a jumpbuf to the thread local stack, ensuring each thread can have it's own
 * error handler.
 *
 * Since each thread is building its own stack, using automatic variables (stack based data) along the way,
 * their needs to be a mechanism for cleaning up the stack and keeping it balanced. Using a THROW to pop
 * the data and call longjmp seems like a valid approach, but has one issue, if code in the TRY block
 * never executes a THROW, the stack never gets updated. On a subsequent THROW, the head is now pointing
 * to an invalid location and a sigsegv will ensue. Instead of this approach, the FINALLY block does
 * the resulting pop to balance the stack. This means that the FINALLY block must always run, this is
 * why limitations of 1 and 3 exist. Ie code cannot exit the CATCH or the FINALLY will never run. This
 * means you cannot use your own setjmp, longjump calls if you cannot guarntee that the return point is
 * in the CATCH block. You calso cannot use goto and leave the CATCH block that way. This is also why if
 * you would like to THROW, you must use CTHROW, wich pops from the stack.
 *
 * The macro functionality is something like this:
 *
 * TRY      --> Push jmpbuf to stack
 * CATCH    --> Switch statement (each CATCH(e) being a case on e.
 * CATCHALL --> Using a gnu C extension, captures the range of 1...MAX_INT
 * FINALLY  --> Default case in the switch that all jmps fall through too. Pops and dicards the
 *              head of the stack, keeping it balanced.
 * CTHROW   --> Pop from the stack, then peek and use that as the result to longjmp
 * THROW    --> Peek the stack and use that as the result to longjmp.
 *
 *
 */

/**
 * Pushes a jump_buf node to the head of the stack
 * @param env
 *  The environment (ie jump buf) to push
 * @warning
 *  DO NOT CALL THIS FUNCTION DIRECTLY ... don't do it..hey.. I said don't
 */
static void __error_push(_cexception_stack *env) {

	_cexception_stack **head =
		(__exception__.head) ? &(__tls_exception__.head) : &(__exception__.head);

	if(head && *head) {
		env->next = *head;
	}
	*head = env;

	return;
}

/**
 * Pop's or peeks a jump buf from the stack
 * @param pop
 *  Whether or not to pop or peek
 * @return
 *  The pointer to the jump buf
 * @warning
 *  DO NOT CALL THIS FUNCTION DIRECTLY .. if you're looking at these it better be for
 *  information only. If you mess up the error handling stack (or the actual call stack)
 *  you're on your own champ :-P
 */
_cexception_stack *__error_pop(_cexception_stack_action pop) {

	_cexception_stack *tmp = NULL;
	_cexception_stack **head = __tls_exception__.head ? &(__tls_exception__.head) : &(__exception__.head);

	if(head && *head) {
		tmp = *head;
		if(pop == _cexception_stack_pop) {
			*head = (*head)->next;
		}
	}

	return tmp;
}

/**
 * Sets the current error to the proper stack
 * @param line
 *  Current line number
 * @param file
 *  Current file
 * @param func
 *  Current function
 * @param e
 *  The error code
 */
static void __set_active_error(int line, const char *file, const char *func, cexception e) {

	_cexception *err = __tls_exception__.head ? &(__tls_exception__) : &(__exception__);

	err->curr_err.active = 1;
	err->curr_err.line = line;
	err->curr_err.func = func;
	err->curr_err.file = file;
	err->curr_err.num = e;
	return;
}

/*
 * define CEXCEPTION_RT before you include this header if you would like to use the protected
 * run time environment. You cannot declare main in your application code, you can change
 * your invocation of main to cexception_start(int argc, char *argv[])
 *
 */
#ifdef CEXCEPTION_RT

extern void cexception_start(int argc, char *argv[]);

int main(int argc, char *argv[]) {


	TRY {
		cexception_start(argc, argv);
	}
	CATCHALL {
		cexception_panic(exception);
	}
	FINALLY {
		exit(EXIT_SUCCESS);
	}
	ETRY;

	/* Never gets called, keeps compiler happy */
	return 0;
}

#endif

#endif /* CEXCEPTION_H_ */
