#include <stdio.h>
#include <pthread.h>

#define CEXCEPTION_RT
#include "cexception.h"

static void foo() {

	THROW(cexception_eaddrnotavail);
	printf("This code never runs\n");
}

static void *thread_func_handler(void *p) {

	TRY {
		printf("Thread throwing error back to thread handler\n");
		THROW(cexception_eaddrnotavail);
	}
	CATCHALL {
		printf("Thread caught exception\n");
		cexception_dump_error();
	}
	FINALLY {
		printf("Thread Finally Executing\n");
	}
	ETRY;
	return NULL;
}

void cexception_start(int argc, char *argv[]) {

	int rc;
	pthread_t thread;

	/* Example of a handled exception */
	TRY {
		foo();
	}
	CATCH(cexception_eaddrnotavail) {
		printf("Caught exception 1\n");
		cexception_dump_error();
	}
	FINALLY {
		printf("Finally 1 executing\n");
	}
	ETRY;

	/* Example of a un-handled exception that disappears (Don't do this)*/
	TRY {
		foo();
	}
	CATCH(cexception_echild) {
		printf("Didn't catch exception 2\n");
		cexception_dump_error();
	}
	FINALLY {
		printf("Finally 2 executing, error went un-handled\n");
	}
	ETRY;

	/* Example of nested throw/catches */
	TRY {
		TRY {
			THROW(cexception_echild);
		}
		CATCHALL {
			printf("Caught exception 3\n");
			cexception_dump_error();
			CTHROW(cexception_edeadlk);
		}
		FINALLY {
			printf("Finally doesn't run\n");
		}
		ETRY;
	}
	CATCHALL {
		printf("Caught exception 4\n");
		cexception_dump_error();
	}
	FINALLY {}
	ETRY;

	/* Example of using errno on a throw */
	TRY {
		FILE * fp = fopen("file not found", "r");
		if(!fp) {
			THROW(errno);
		}
	}
	CATCHALL {
		printf("Caught exception 5\n");
		cexception_dump_error();
	}
	FINALLY {
		printf("Finally 5 executing\n");
	}
	ETRY;

	/* TEST Thread handling */
	TRY {
		printf("Starting thread...\n");
		rc = pthread_create(&thread, NULL, thread_func_handler, NULL);
		if(rc) {
			THROW(errno);
		}
	}
	CATCHALL {

	}
	FINALLY {}
	ETRY;

	/* Example of a handled exception, that throws another exception */
	TRY {
		THROW(cexception_einval);
	}
	CATCHALL { //Notice that this will catch any error
		printf("Caught exception 6: %d\n", exception); //Notice you can get the exception number as well
		cexception_dump_error();

		pthread_join(thread, NULL);

		/*
		 * A THROW in a CATCH block must be a CTHROW
		 * Also, since their is no encompassing TRY/THROW
		 * and we compiled with CEXCEPTION_RT set, this
		 * will get sent to the global TRY CATCH, and a
		 * panic message will be printed.
		 */
		CTHROW(cexception_efault);
	}
	FINALLY {
		printf("Finally 6 WILL NOT execute\n");
	}
	ETRY;

	return;
}
